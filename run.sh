#!/bin/bash

IMAGE_NAME=`basename $PWD`

docker build -t "$IMAGE_NAME" .
docker run --rm -v $PWD:/home/docker/project -v ~/.m2:/home/docker/.m2 -v ~/.gradle:/home/docker/.gradle -it "$IMAGE_NAME" ./gradlew $*
