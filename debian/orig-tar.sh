#!/bin/sh -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2

if [ "$VERSION" = "" ] ; then
      VERSION=`dpkg-parsechangelog | awk '/^Version:/ { print $2 }' | sed 's/\([0-9\.]\+\)+[dfsg0-9.]\+-[0-9]\+$/\1/'`
fi

DIR=${PACKAGE}-${VERSION}+dfg
TAR=../${PACKAGE}_${VERSION}+dfsg.orig.tar.xz

svn export http://svn.svnkit.com/repos/3rdparty/de.regnis.q.sequence/tags/$VERSION $DIR
tar -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '.settings' \
    --exclude 'gradle' \
    --exclude 'gradlew*' \
    $DIR
rm -rf $DIR $3
